import React from 'react';
import './App.less';
import MiniCalculator from "./componenets/MiniCalculator";

const App = () => {
    return <div className="App">
        <MiniCalculator/>
    </div>
};

export default App;
