import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: 0,
    };
    this.plusOneHandler = this.plusOneHandler.bind(this);
    this.minusOneHandler = this.minusOneHandler.bind(this);
    this.multiplyTwoHandler = this.multiplyTwoHandler.bind(this);
    this.divideTwoHandler = this.divideTwoHandler.bind(this);
  }

  plusOneHandler() {
    this.setState({
      result: this.state.result + 1
    });
  }

  minusOneHandler() {
    this.setState({
      result: this.state.result - 1
    });
  }

  multiplyTwoHandler() {
    this.setState({
      result: this.state.result * 2
    });
  }

  divideTwoHandler() {
    this.setState({
      result: this.state.result / 2
    })
  }
  
  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result">{this.state.result}</span>
        </div>
        <div className="operations">
          <button onClick={this.plusOneHandler}>加1</button>
          <button onClick={this.minusOneHandler}>减1</button>
          <button onClick={this.multiplyTwoHandler}>乘以2</button>
          <button onClick={this.divideTwoHandler}>除以2</button>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

